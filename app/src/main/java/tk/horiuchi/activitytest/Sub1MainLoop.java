package tk.horiuchi.activitytest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.view.SurfaceView;

/**
 * Created by yoshimine on 2017/07/25.
 */

public class Sub1MainLoop extends MainLoopBase {
    public Sub1MainLoop(Context context, SurfaceView sv) {
        super(context, sv);
    }

    @Override
    protected void doDraw() {
        Canvas canvas = holder.lockCanvas();
        if(canvas == null){
            return;
        }

        //丸の表示位置を動かす
        if( circle_x < 0 || circle_x > this.width ){
            dx = -dx;
        }
        if( circle_y < 0 || circle_y > this.height ){
            dy = -dy;
        }

        circle_x += dx;
        circle_y += dy;

        //描画処理を開始
        canvas.drawColor(0, PorterDuff.Mode.CLEAR );
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        canvas.drawCircle( circle_x, circle_y, 50, paint);

        //描画処理を終了
        holder.unlockCanvasAndPost(canvas);

        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
        }

    }
}
