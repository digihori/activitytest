package tk.horiuchi.activitytest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by yoshimine on 2017/07/25.
 */

public class MainLoopBase extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    protected SurfaceHolder holder = null;
    protected Thread _thread = null;

    protected float dx = 10, dy = 10;
    protected float width, height;
    protected int   circle_x, circle_y;


    public MainLoopBase(Context context, SurfaceView sv) {
        super(context);
        holder = sv.getHolder();
        holder.addCallback(this);
        Log.w("LOG", "SurfaceView created!!");

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //解像度情報変更通知
        this.width = width;
        this.height = height;
        Log.w("LOG", "surfaceChanged!!");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        this.holder = holder;
        _thread = new Thread(this);             //別スレッドでメインループを作る
        _thread.start();

        Log.w("LOG", "surfaceCreated!!");

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        _thread = null;

        Log.w("LOG", "surfaceDestroyed!!");
    }

    @Override
    public void run() {
        //Log.w("LOG", "!!!run!!");
        while (_thread!=null) { //メインループ
            //Log.w("LOG", "run!!");
            doDraw();
        }
    }

    protected void doDraw() {
        Canvas canvas = holder.lockCanvas();
        if(canvas == null){
            return;
        }

        //丸の表示位置を動かす
        if( circle_x < 0 || circle_x > this.width ){
            dx = -dx;
        }
        if( circle_y < 0 || circle_y > this.height ){
            dy = -dy;
        }

        circle_x += dx;
        circle_y += dy;

        //描画処理を開始
        canvas.drawColor(0, PorterDuff.Mode.CLEAR );
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        canvas.drawCircle( circle_x, circle_y, 50, paint);

        //描画処理を終了
        holder.unlockCanvasAndPost(canvas);

        try {
            Thread.sleep(15);
        } catch (InterruptedException e) {
        }

    }

}
