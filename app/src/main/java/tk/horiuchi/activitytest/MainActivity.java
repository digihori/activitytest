package tk.horiuchi.activitytest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.send_button1).setOnClickListener(this);
        findViewById(R.id.send_button2).setOnClickListener(this);
    }

    public void onClick(View v) {

        int c = v.getId();
        Intent intent;

        switch (c) {
            default:
            case R.id.send_button1:
                intent = new Intent(getApplication(), Sub1Activity.class);
                break;
            case R.id.send_button2:
                intent = new Intent(getApplication(), Sub2Activity.class);
                break;
        }
        startActivity(intent);
    }
}
