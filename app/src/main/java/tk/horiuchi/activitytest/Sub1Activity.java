package tk.horiuchi.activitytest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by yoshimine on 2017/07/25.
 */

public class Sub1Activity extends SubActivityBase {
    private MainLoopBase mainloop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub1);
        findViewById(R.id.return_button).setOnClickListener(this);

        SurfaceView sv = (SurfaceView) findViewById(R.id.surfaceView);
        mainloop = new Sub1MainLoop(this, sv);

    }

}
