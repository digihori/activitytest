package tk.horiuchi.activitytest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by yoshimine on 2017/07/25.
 */

public class SubActivityBase extends AppCompatActivity implements View.OnClickListener {
    //private MainLoopBase mainloop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void onClick(View v) {
        finish();
    }

}
